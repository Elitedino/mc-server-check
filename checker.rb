require 'net/http'
require 'json'
require 'chilkat'
url = 'https://api.mcsrvstat.us/2/IP:PORT'
uri = URI(url)
response = Net::HTTP.get(uri)
jsonStr = JSON.parse(response)
file = File.new("data.json","w+")
file.puts(JSON.pretty_generate(jsonStr))
file.close
puts "JSON from website printed into data.json"